
<h2>Liste des biens à louer:</h2>
<div class="d-flex justify-content-around">

    <?php foreach( $leasings as $leasing): ?>
    
        <div class="card" style="width: 20rem;">
            <img src="<?php echo '/images/'.$leasing->getPhoto() ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Pays : <?php echo $leasing->getAddress()->getPays() ?></h5>                
                <h5 class="card-title">Ville : <?php echo $leasing->getAddress()->getVille() ?></h5>                
                <p class="card-text">Type de logement : <?php echo $leasing->getHousing()->getType() ?></p>
                <p class="card-text">Surface:  <?php echo $leasing->getSuperficie() ?> m²</p>
                <p class="card-text">Nombre de couchage:  <?php echo $leasing->getCouchage() ?></p>
                <p class="card-text">Descriptif:  <?php echo $leasing->getDescription() ?></p>
                <p class="card-text">prix à la semaine:  <?php echo $leasing->getPrix() ?> €</p>
                <a href="leasing/detail/<?php echo $leasing->getId() ?>" class="btn btn-primary">Détails</a>                
            </div>
        </div>      

    <?php endforeach ?>

</div>