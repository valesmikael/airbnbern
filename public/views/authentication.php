<?php include 'partials/errors.php' ?>

<div class="container">
    <div class="row">
        <div class="col">        
            <h2> Login </h2>
                <form action="/login" method="POST">
                    <div class="form-group">
                        <label>
                            <span> Username </span>
                            <input type="text" name="username">
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            <span> Password </span>
                            <input type="password" name="password">
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            <input type="submit" class="btn btn-primary" value="Log in">
                        </label>
                    </div>

                </form>
        </div>

        <div class="col">
            <h2> Signin </h2>
                <form action="/signin" method="POST">

                    <div class="form-group">
                        <label>
                            <span> Username </span>
                            <input type="text" name="username">
                        </label>
                    </div>

                    <div class="form-group">                    
                        <label>
                            <span> Password </span>
                            <input type="password" name="password">
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            <span> Confirm Password </span>
                            <input type="password" name="password_check">
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            <span> Prénom </span>
                            <input type="text" name="firstname">
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            <span> Nom </span>
                            <input type="text" name="name">
                        </label>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                            <input type="radio" aria-label="Radio button for following text input" name="role_id" value="1">
                            </div>
                            <label class="form-check-label" >Cochez si vous souhaitez déposer des annonces</label><br>

                            <div class="input-group-text">
                            <input type="radio" aria-label="Radio button for following text input" name="role_id" value="2" checked="checked">
                            </div>
                            <label class="form-check-label" >Cochez si vous souhaitez simplement louer</label>
                        </div>                        
                    </div>

                    <label>
                        <input type="submit" class="btn btn-primary" value="Sign in">
                    </label>

                </form>
        </div>

    </div>

</div>