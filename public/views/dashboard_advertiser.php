<h2>Bienvenu sur votre espace perso</h2>

<h3>Vous proposez actuellement les biens suivants:</h3>

<div class="d-flex justify-content-around">

    <?php foreach( $houses as $house): ?>
    
        <div class="card" style="width: 20rem;">
            <img src="<?php echo '/images/'.$house->getPhoto() ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Pays : <?php echo $house->getAddress()->getPays() ?></h5>                
                <h5 class="card-title">Ville : <?php echo $house->getAddress()->getVille() ?></h5>                
                <p class="card-text">Type de logement : <?php echo $house->getHousing()->getType() ?></p>
                <p class="card-text">Surface:  <?php echo $house->getSuperficie() ?> m²</p>
                <p class="card-text">Nombre de couchage:  <?php echo $house->getCouchage() ?></p>
                <p class="card-text">Descriptif:  <?php echo $house->getDescription() ?></p>
                <p class="card-text">prix à la semaine:  <?php echo $house->getPrix() ?> €</p>                
            </div>
        </div>      

    <?php endforeach ?>

</div>

<h3>Vos biens suivant ont été reservés:</h3>

    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col"></th>
            <th scope="col">Date de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Voir détail du bien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $myAllReservations as $myAllReservation): ?>
                <tr>
                
                <th><img src="<?php echo '/images/'.$myAllReservation->getPhoto() ?>" alt="" class="stick"></th>
                <td><?php echo $myAllReservation->formatDateStart() ?></td>
                <td><?php echo $myAllReservation->formatDateEnd() ?></td>
                <td><a href="/leasing/detail/<?php echo $myAllReservation->getDetail_id() ?>">Voir le Descriptif</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>


    
</table>

<h3>Vous avez réservé les biens suivant:</h3>


    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col"></th>            
            <th scope="col">Date de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Voir détail du bien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $housesLeasings as $housesLeasing): ?>
                <tr>
                
                <th><img src="<?php echo '/images/'.$housesLeasing->getPhoto() ?>" alt="" class="stick"></th>                
                <td><?php echo $housesLeasing->formatDateStart() ?></td>
                <td><?php echo $housesLeasing->formatDateEnd() ?></td>
                <td><a href="/leasing/detail/<?php echo $housesLeasing->getDetail_id() ?>">Voir le Descriptif</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    
<h3>Vos favories</h3>


    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col"></th>
            <th scope="col">Voir détail du bien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $favoriesLeasings as $favoryLeasing): ?>
                <tr>                
                <th><img src="<?php echo '/images/'.$favoryLeasing->getPhoto() ?>" alt="" class="stick"></th> 
                <td><a href="/leasing/detail/<?php echo $favoryLeasing->getDetail_id() ?>">Voir le Descriptif</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>     



