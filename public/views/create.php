<?php include 'partials/errors.php' ?>

<div class="card" style="width: 50rem;">
    <form action="/leasing/store" method="post" enctype="multipart/form-data">

        <h3>Creer votre annonce :</h3>

        <div class="form-group">
            <label> Pays </label>
            <input type="text" name="pays" class="form-control" placeholder="Saisissez le pays de résidence">
        </div>

        <div class="form-group">
            <label> Ville </label>
            <input type="text" name="ville" class="form-control" placeholder="Saisissez la ville de résidence">
        </div>

        <div class="form-group">
            <label> Entrer un descriptif qui attire la curiosité </label>
            <textarea class="form-control" name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect1">Type de logement</label>
                <select class="form-control" name="housing_id">
                    <?php foreach( $types as $type ): ?>
                        <option value="<?php echo $type->getId() ?>"><?php echo $type->getType() ?></option>    
                    <?php endforeach ?>
                </select>
        </div>

        <div class="form-group">
            <label> Quelle est la superficie du logement </label>
            <input type="text" name="superficie" class="form-control" placeholder="Saisissez la superficie du logement">
        </div>

        <div class="form-group">
            <label> Quelle est le nombre de couchage du logement </label>
            <input type="text" name="couchage" class="form-control" placeholder="Saisissez le nombre de couchage">
        </div>

        <div class="form-group">
            <label> Tarif à la semaine en euros</label>
            <input type="text" name="prix" class="form-control" placeholder="Prix en €">
        </div>

        <div class="form-group">
            <label> Début de mise en location</label>
            <input type="Date" name="datedebut" class="form-control"">
        </div>

        <div class="form-group">
            <label> Début de fin de mise en location</label>
            <input type="Date" name="datefin" class="form-control"">
        </div>

        <div class="form-group">
            <label> Ajouter une photo</label>
            
            <input type="file" name="photo" class="form-control">
        </div>
                

        <h4>Equipement</h4>
        <?php foreach( $equipments as $equipment ): ?>

        <label>
            <span> <?php echo $equipment->getLabel() ?> </span>
            <input type="checkbox" value="<?php echo $equipment->getId() ?>" name="equipments[]">
        </label><br>

        <?php endforeach ?>

        <button type="submit" class="btn btn-primary">proposer le bien</button>
    </form>
    
</div>