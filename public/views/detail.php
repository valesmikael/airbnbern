<h2>Annonce postée par 
    <span>
        <?php echo $leasing->getUser()->getFirstname()?> <?php echo $leasing->getUser()->getName()?>
    </span>
</h2>

<div class="card" style="width: 40rem;">
            <img src="<?php echo '/images/'.$leasing->getPhoto() ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Pays : <?php echo $leasing->getAddress()->getPays() ?></h5>                
                <h5 class="card-title">Ville : <?php echo $leasing->getAddress()->getVille() ?></h5>                
                <p class="card-text">Type de logement : <?php echo $leasing->getHousing()->getType() ?></p>
                <p class="card-text">Surface:  <?php echo $leasing->getSuperficie() ?> m²</p>
                <p class="card-text">Nombre de couchage:  <?php echo $leasing->getCouchage() ?></p>
                <p class="card-text">Descriptif:  <?php echo $leasing->getDescription() ?></p>
                <!-- <p class="card-text">Du:  <?php echo $leasing->getDatedebut() ?></p>
                <p class="card-text">Au:  <?php echo $leasing->getDatefin() ?></p> -->
                <p class="card-text">prix à la semaine:  <?php echo $leasing->getPrix() ?> €</p>

                <h4>Le logement est équipé de :</h4>
                <ul>
                <?php foreach ( $leasing->getEquipments() as $equipment ): ?>
                    <li><?php echo $equipment->getLabel()?></li>
                <?php endforeach ?>
                </ul>

                <h3>Reservation :</h3>
                <form action="/leasing/reserved/<?php echo $leasing->getId() ?>" method="POST">
                    <input type="hidden" name="detail_id" value="<?php echo $leasing->getId() ?>">
                    <div class="form-group">
                        <label> Du</label>
                        <input type="Date" name="datestart" class="form-control"">

                        <label> Au </label>
                        <input type="Date" name="dateend" class="form-control"">
                    </div> 

                    <button type="submit" class="btn btn-primary">Réserver</button>                   
                   
                </form> 

                <form action="/leasing/favories/<?php echo $leasing->getId() ?>" method="POST">
                    <input type="hidden" name="detail_id" value="<?php echo $leasing->getId() ?>">
                    <button type="submit" class="btn btn-success">ajouter à mes favories</button>
                </form> 
                   
            </div>
</div>  