<h2>Bienvenu sur votre espace perso</h2>

<h3>Vous avez réservé les biens suivant:</h3>


    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col"></th>
            <th scope="col">Date de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Voir détail du bien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $housesLeasings as $housesLeasing): ?>
                <tr>
                
                <th><img src="<?php echo '/images/'.$housesLeasing->getPhoto() ?>" alt="" class="stick"></th> 
                <td><?php echo $housesLeasing->formatDateStart() ?></td>
                <td><?php echo $housesLeasing->formatDateEnd() ?></td>
                <td><a href="/leasing/detail/<?php echo $housesLeasing->getDetail_id() ?>">Voir le Descriptif</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

<h3>Vos favories</h3>


    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col"></th>
            <th scope="col">Voir détail du bien</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $favoriesLeasings as $favoryLeasing): ?>
                <tr>                
                <th><img src="<?php echo '/images/'.$favoryLeasing->getPhoto() ?>" alt="" class="stick"></th> 
                <td><a href="/leasing/detail/<?php echo $favoryLeasing->getDetail_id() ?>">Voir le Descriptif</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    
    



