<?php if ( !empty($errors) ):?>

    <div >

        <?php foreach ($errors as $error): ?>

            <div class="alert alert-danger" role="alert"><?php echo $error ?></div>

        <?php endforeach ?>
    
    
    </div>

<?php endif ?>