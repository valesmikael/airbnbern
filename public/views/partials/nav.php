<ul class="nav justify-content-end">

    <li class="nav-item">
      <a class="nav-link active" href="/">Accueil</a>
    </li>

  <?php if (Auth::isLogged() && Auth::user()->hasRole( Role::ADVERTISER) ): ?>
    <li class="nav-item">
      <a class="nav-link active" href="/leasing/create">Créer une annonce</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" href="/dashboard/advertiser">Espace perso</a>
    </li>
    <li>
    <a class="nav-link" href="/unlogin">se déconnecter</a>
    </li>
  <?php endif ?>

  <?php if (Auth::isLogged() && Auth::user()->hasRole( Role::STANDARD) ): ?>    
    <li class="nav-item">
      <a class="nav-link active" href="/dashboard/standard">Espace perso</a>
    </li>
    <li>
    <a class="nav-link" href="/unlogin">se déconnecter</a>
    </li>
  <?php endif ?>

    <li class="nav-item">
      <a class="nav-link" href="/authentication">Login/signin</a>
    </li>

</ul>