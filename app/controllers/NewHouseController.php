<?php

class NewHouseController extends Controller {

    function create() {

        $equipments = Equipment::findAll();
        $types = TypeHousing::findAll();

        $this->render('create',compact('equipments','types'));

    }

    function store() {        

        if( $this->checkFields( ['pays','ville','housing_id','description','superficie','couchage','prix','equipments','datedebut','datefin'] ) ) {

                        
            $address = new Address( $this->fields);            
            $leasingHouse = new LeasingHouse( $this->fields);
                
                
            if(!empty($_FILES['photo'])) {

                $img = $_FILES['photo'];                
                
                $imageName = $leasingHouse->renameAndRedirectPhoto($img);
                if(empty($imageName)){

                    $this->addError('Probleme lors de la création de l\'image');
                    $this->redirect('/leasing/create'); 
                } else {

                    $leasingHouse->setPhoto($imageName);
                }
                
            } else {

                $this->addError('Votre fichier n\'est pas une image');
                $this->redirect('/leasing/create');

            }
                
        }
            
            if ($address->create() ) {
                //l'objet Address étant créé, on assigne l'id de l'objet a $leasingHouse grace au setAddress_id                
                
                $leasingHouse->setAddress_id($address->getId());              
                $leasingHouse->setUser_id($_SESSION['user']->getId());
                

                    if( $leasingHouse->create() ) {
        
                        foreach( $this->fields['equipments'] as $equipment_id ) {
                            $leasingHouse->addEquipmentById( $equipment_id );
                            
                        }
        
                        $leasingHouse->linkEquipments();
        
                        $this->redirect('/');
                    }
                

            }

    }


}