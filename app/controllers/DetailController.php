<?php

class DetailController extends Controller {

   

    function show( int $id ) {

        $leasing = new LeasingHouse;
        
        
        if( $leasing->find( $id ) ){

            
            $this->render('detail', compact( 'leasing' ) );
        }
            
        else 

           $this->notFound(); 
    
    }

    function create() {

        $equipments = Equipment::findAll();

        $this->render('create',compact('equipments'));

    }


}