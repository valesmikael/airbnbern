<?php
abstract class CheckAdvertiserStandardController extends checkController {

    protected function canAccess():bool {

        return Auth::isLogged() 
            && ( Auth::user()->hasRole( Role::ADVERTISER  ) || Auth::user()->hasRole(Role::STANDARD) );
            
    }

    protected function errorAction() {

        $this->addError('Vous n\'est pas autorisé à accéder à cette page');
        $this->redirect('/authentication');

    }
}