<?php
class AuthController extends Controller {

    public function form() {

        $this->render('authentication');
        
    }

    public function login() {

        if( $this->checkFields(['username', 'password']) ) {

            $auth = new Auth( $this->fields['username'], $this->fields['password'] );

            if( $user = $auth->login() ) 
                $this->redirect('/');
            else 
                $this->addError( 'Login ou mot de passe incorrect !');

        }
        else $this->addError( 'Il manque des champs' );

        $this->redirect('/authentication');

    }

    public function signin() {

        if( $this->checkFields( ['username', 'password', 'password_check', 'firstname', 'name', 'role_id'] ) ) {

            $auth = new Auth(
                $this->fields['username'],
                $this->fields['password'],
                $this->fields['password_check'],
                $this->fields['firstname'],
                $this->fields['name'],
                $this->fields['role_id']
            );

            if( $auth->userExists() )
                $this->addError('L\'utilisateur existe déjà');

            elseif( !$auth->checkPasswords() )
                $this->addError('Les mots de passe ne correspondent pas');

            else if( $user = $auth->signin() )
                $this->redirect('/');

            else 
                $this->addError('Une erreur est survenue');

        }
        else $this->addError('Il manque des champs');
        
        $this->redirect('/authentication');
    }

    public function unlogin() {

        unset( $_SESSION['user']);

        $this->redirect('/');
        
    }

}