<?php 

class DashboardAdvertiserController extends CheckAdvertiserStandardController  {

    function index() {

        $this->checkAccess();

        $leasings = new LeasingHouse;        
        $houses = $leasings->findByUser($_SESSION['user']->getId());

        $reservations = new Reserved;
        $housesLeasings = $reservations->findByUser($_SESSION['user']->getId());

        $myReservations = new Reserved;
        $myAllReservations = $myReservations->findReservedOfUserId($_SESSION['user']->getId());

        $favories = new Favories;
        $favoriesLeasings = $favories->findByUser($_SESSION['user']->getId());

        
        $this->render('dashboard_advertiser', compact( 'houses', 'housesLeasings', 'myAllReservations', 'favoriesLeasings' ) );

    }

}