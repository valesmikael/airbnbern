<?php

abstract class Controller {

    protected $title = APP_NAME;
    protected $errors = [];
    
    protected $fields = [];

    public function __construct()

    {
        // envoie à l'affichage si il y en a selon la fonction utilisé
        if ( !empty($_SESSION['errors']) )
            $this->errors = $_SESSION['errors'];

        // stockage des données envoyé en POST dans la variable $this->fields
        $this->fields = $_POST;
    }

    //vérification des champs envoyé
    protected function checkfields( array $params ): bool {

        foreach ( $params as $param ) {

            if( empty($this->fields[ $param ]) ){
                return false;
            }

        }

        return true;

    }

    // ajout de nouvelle erreurs
    protected function addError(string $error) {

        $this->errors[] = $error;
        $_SESSION['errors'] = $this->errors; 

    }

    // rendu de l'affichage
    protected function render(string $view, array $vars = [] ) {

        $vars ['title'] = $this->title;
        $vars ['errors'] = $this->errors;

        // Extrait un tableau associatif et transforme
        // tout en variable $
        extract( $vars );

        //affichage des vues
        include_once "public/views/partials/header.php";
        include_once "public/views/partials/nav.php";
        include_once "public/views/{$view}.php";
        include_once "public/views/partials/footer.php";

        unset( $_SESSION['errors'] );

        die;

    }

    // renvoi d'une page 404 grace a la function notFound de flight

    protected function notFound(){

        Flight::notFound();
        die;

    }

    // fonction de redirection

    protected function redirect( string $route) {

        Flight::redirect( $route );
        die;

    }


}