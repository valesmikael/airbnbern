<?php

abstract class CheckController extends Controller {

    abstract protected function canAccess():bool;
    abstract protected function errorAction();

    protected function checkAccess() {

        if( !$this->canAccess() ) $this->errorAction();

    }

}