<?php 

class ReservedController extends Controller {

    function store() {        

        if( $this->checkFields( ['datestart','dateend','detail_id'] ) ) {

           
            $reserved = new Reserved( $this->fields);
            $reserved->setUser_id($_SESSION['user']->getId());            
            
           
            if ($reserved->create() ) {
               //l'objet Address étant créé, on assigne l'id de l'objet a $reserved grace au setUser_id              
              
               $this->redirect('/');
            }
                       
       }

   }
}