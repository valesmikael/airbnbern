<?php 

class DashboardStandardController extends CheckAdvertiserStandardController  {

    function index() {

        $this->checkAccess();

        $reservations = new Reserved;
        $housesLeasings = $reservations->findByUser($_SESSION['user']->getId());

        $favories = new Favories;
        $favoriesLeasings = $favories->findByUser($_SESSION['user']->getId());


        
        $this->render('dashboard_standard', compact( 'housesLeasings', 'favoriesLeasings') );

    }

}