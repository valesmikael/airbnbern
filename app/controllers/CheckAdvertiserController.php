<?php

abstract class CheckAdvertiserController extends CheckController {

    protected function canAccess():bool {

        return Auth::isLogged() && Auth::user()->hasRole( Role::ADVERTISER );
            
    }

    protected function errorAction() {

        $this->addError('Vous n\'est pas autorisé à accéder à cette page');
        $this->redirect('/authentication');

    }
}