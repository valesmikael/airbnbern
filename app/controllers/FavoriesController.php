<?php 

class FavoriesController extends Controller {

    function store() {        

        if( $this->checkFields( ['detail_id'] ) ) {

           
            $favory = new Favories( $this->fields);
            $favory->setUser_id($_SESSION['user']->getId());             
                       
           
            if ($favory->create() ) {
               //l'objet Address étant créé, on assigne l'id de l'objet a $reserved grace au setUser_id              
              
               $this->redirect('/');
               
            }
                       
       }

   }
}