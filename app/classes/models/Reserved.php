<?php

class Reserved extends Model {

    private $datestart;
    private $dateend;

    private $detail_id;
    private $user_id;

    private $detail = NULL;
    private $user = NULL;

    protected function getTable(): string {
        return 'reservations';
    }
   
    protected function toArray(): array {
        return [
            'datestart' => $this->datestart,                       
            'dateend' => $this->dateend,
            'detail_id' => $this->detail_id,
            'user_id' => $this->user_id                       
        ];
    }


    public function getDatestart() {
        return $this->datestart;
    }
     
    public function setDatestart($datestart) {
        $this->datestart = $datestart;

        return $this;
    }
    
    public function getDateend() {
        return $this->dateend;
    }

    public function setDateend($dateend) {
        $this->dateend = $dateend;

        return $this;
    }
     
    public function getDetail_id() {
        return $this->detail_id;
    }

    public function setDetail_id($detail_id) {
        $this->detail_id = $detail_id;

        return $this;
    }
 
    public function getUser_id() {
        return $this->user_id;
    }
    
    public function setUser_id($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    public function findByUser(int $id) {

        $this->id=$id;

        // $sql="SELECT {$this->getTable()}.* FROM {$this->getTable()}
        //         WHERE user_id= :id";
        $sql="SELECT * FROM details 
                JOIN {$this->getTable()} as res
                ON res.detail_id = details.id
                WHERE res.user_id =:id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute([ 'id' => $this->id ]);
        $results = $stmt->fetchAll();
        
        $reservations = [];

        foreach( $results as $result){

            $reservation = new Reserved( $result );
        
            $reservations[] = $reservation;
        }

        return $reservations;

    }
    public function findReservedOfUserId(int $id) {
        $this->id=$id;

        $sql="SELECT * FROM {$this->getTable()} 
                JOIN details
                ON details.id = reservations.detail_id
                WHERE details.user_id =:id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute([ 'id' => $this->id ]);
        $results =$stmt->fetchAll();

        $reservations = [];

        foreach( $results as $result ) {
            $reservation = new Reserved( $result );
            $reservations[] = $reservation;
        }

        return $reservations;

    }

    public function formatDateStart() { 
        
        $date = new DateTime($this->getDatestart());
        return $date->format('d-m-Y');                 

    }

    public function formatDateEnd() { 
        
        $date = new DateTime($this->getDateend());
        return $date->format('d-m-Y');                 

    }
    
}