<?php

class User extends Model {

    private $username;
    private $password;
    private $firstname;
    private $name;

    private $role_id;

    protected function getTable(): string {
        return 'users';
    }

    protected function toArray(): array {
        return [
            'username' => $this->username,
            'password' => $this->password,
            'firstname' => $this->firstname,
            'name' => $this->name,            
            'role_id' => $this->role_id            
        ];
    }

    public function getUsername(): string {
        return $this->username;
    }

    public function setUsername( $username) {
        $this->username = $username;
        return $this;

    }

    public function getPassword(): string  {
        return $this->password;
    }

    public function setPassword( $password ){
        $this->password = $password;
        return $this;
    }

    public function getFirstname(): ?string {
        return $this->firstname;
    }

    public function setFirstname( $firstname ) {
        $this->firstname = $firstname;
        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName( $name ) {
        $this->name = $name;
        return $this;
    }

    public function getRoleId(): int {
        return $this->role_id;
    }

    public function setRoleId( $role_id ) {
        $this->role_id = $role_id;
        return $this;
    }

    public function findUserByDetailId( $id ): bool {

        $this->id = $id;
        
        $sql="SELECT {$this->getTable()}.* FROM {$this->getTable()}
        JOIN details ON user_id = users.id
        where user_id = :id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute(['id'=>$this->id]);
        $results = $stmt->fetch();
      
        if( !$results ) return false;
        
        $this->hydrate( $results );
        return true;


    }

    public function hasRole( string $role ):bool {

        $sql = 'SELECT role_id FROM users WHERE id=:id';
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute(['id' => $this->id]);
        $result = $stmt->fetch();

        return $role == $result['role_id'];
    }

}