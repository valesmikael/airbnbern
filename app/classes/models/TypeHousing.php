<?php

class TypeHousing extends Model {

    private $typeHousing;

    protected function getTable(): string {
        return 'housings';
    }
   
    protected function toArray(): array {
        return [
            'type' => $this->type,                       
        ];
    }

    public function getType(): ?string {
        return $this->typeHousing;
    }


    public function setType( $typeHousing ) {
        $this->typeHousing = $typeHousing;
        return $this;
    }


    public function findTypeByDetailId( $id ): bool {

        $this->id = $id;
        
        $sql="SELECT {$this->getTable()}.* FROM {$this->getTable()}
        JOIN details ON housing_id = {$this->getTable()}.id
        where housing_id = :id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute(['id'=>$this->id]);
        $results = $stmt->fetch();
      
        if( !$results ) return false;
        
        $this->hydrate( $results );
        return true;


    }

}

