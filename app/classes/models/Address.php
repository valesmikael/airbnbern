<?php

class Address extends Model {

    private $pays;
    private $ville;

    protected function getTable(): string {
        return 'address';
    }

    protected function toArray(): array {
        return [
            'pays' => $this->pays,
            'ville' => $this->ville            
        ];
    }

    public function getPays(): ?string  {
        return $this->pays;
    }

    public function setPays( $pays ){
        $this->pays = $pays;
        return $this;
    }

    public function getVille(): ?string {
        return $this->ville;
    }

    public function setVille( $ville ) {
        $this->ville = $ville;
        return $this;
    }


    public function findAddressByDetailId( $id ): bool {

        $this->id = $id;

        
        $sql="SELECT {$this->getTable()}.* FROM {$this->getTable()}
        JOIN details ON address_id = {$this->getTable()}.id
        where address_id = :id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute(['id'=>$this->id]);
        $results = $stmt->fetch();

        if( !$results ) return false;
        
        $this->hydrate( $results );
        return true;


    }

}