<?php

class LeasingHouse extends Model {
    
    private $superficie;
    private $description;
    private $couchage;
    private $photo;
    private $datedebut;
    private $datefin;
    private $prix;

    
    private $address_id;
    private $housing_id;
    private $user_id;
    
    private $address = null ;
    private $housing = null ;
    private $user = null ;
    
    private $equipments = [] ;

    public function getPhoto() {
        return $this->photo;
    }

    public function setPhoto( $photo ) {        
        $this->photo = $photo;
        return $this;
    }

 
    protected function getTable(): string {
        return 'details';
    }

    protected function toArray(): array {
        return [
            'superficie' => $this->superficie,
            'description' => $this->description,
            'couchage' => $this->couchage,
            'photo' => $this->photo,
            'prix' => $this->prix,
            'datedebut' => $this->datedebut,
            'datefin' => $this->datefin,
            'address_id' => $this->address_id,
            'housing_id' => $this->housing_id,
            'user_id' => $this->user_id
        ];
    }

    public function getAddress(): Address {
        if ( is_null($this->address) ){
            $this->address = new Address;
            $this->address->findAddressByDetailId($this->address_id);
        } 

        return $this->address;
    }

    public function setAddress( Address $address ) {
        $this->address = $address;
        return $this;
    }

    public function getHousing(): TypeHousing {
        if ( is_null($this->housing) ){
            $this->housing = new TypeHousing;
            $this->housing->findTypeByDetailId( $this->housing_id );
        }

        return $this->housing;
        
    }

    public function setHousing( TypeHousing $housing ) {
        $this->housing = $housing;
        return $this;
    }

    public function getUser(): User {
        if ( is_null($this->user) ){
            $this->user = new User;
            $this->user->findUserByDetailId( $this->user_id );
        }

        return $this->user;
        
    }

    public function setUser( User $user ) {
        $this->user = $user;
        return $this;
    }

    public function getEquipments():array {
        
        if( empty ($this->equipments) ) {

            $sql = 'SELECT equipments.* 
                    FROM equipments
                    JOIN equipments_details as ed
                    ON ed.equipment_id = equipments.id
                    WHERE ed.detail_id = :id';

            $stmt = $this->bdd->getPdo()->prepare( $sql );
            $stmt->execute( [ 'id'=>$this->id ] );
            $results = $stmt->fetchAll();


            foreach ( $results as $result ) {
                
                $this->equipments[]= new Equipment($result);
                
            }

        } 
        
        return $this->equipments; 
        
    }
    
    public function setEquipment( Array $equips ) {
        $this->equips = $equips;
        return $this;
    }

    public function getSuperficie(): int {
        return $this->superficie;
    }

    public function setSuperficie( $superficie ) {
        $this->superficie = $superficie;
        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setDescription( $description ) {
        $this->description = $description;
        return $this;
    }

    public function getCouchage(): int {
        return $this->couchage;
    }

    public function setCouchage( $couchage ) {
        $this->couchage = $couchage;
        return $this;
    }

   
    public function getPrix(): string {
        return $this->prix;
    }

    public function setPrix( $prix ) {
        $this->prix = $prix;
        return $this;
    }

    public function getDatedebut() {
        return $this->datedebut;
    }


    public function setDatedebut($datedebut) {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin() {
        return $this->datefin;
    }
    
    public function setDatefin($datefin) {
        $this->datefin = $datefin;

        return $this;
    }

    public function getAddress_id(): int {
        return $this->address_id;
    }

    public function setAddress_id( $address_id ) {
        $this->address_id = $address_id;
        return $this;
    }

    public function getHousing_id(): int {
        return $this->housing_id;
    }

    public function setHousing_id( $housing_id ) {
        $this->housing_id = $housing_id;
        return $this;
    }

    public function getUser_id(): int {
        return $this->user_id;
    }

    public function setUser_id( $user_id ) {
        $this->user_id = $user_id;
        return $this;
    }

    //Permet de lier les categories a son produit dans la bdd
    public function linkEquipments():bool {

        // but générer une requête de type INSERT INTO equipments_details VALUE (1,2), (1,2), etc

        $sql = 'INSERT INTO equipments_details VALUES ';
        foreach( $this->equipments as $equipment ) {
            $sql .= "( {$this->id}, {$equipment->getId()} ), ";
        }

        $sql = rtrim( $sql, ', ' );

        $stmt = $this->bdd->getPdo()->query( $sql );

        return $stmt->rowCount() > 0;

    }

    public function addEquipmentById( int $id ) {
        $equipment = new Equipment;
        if( $equipment->find( $id ) ) {
            $this->equipments[] = $equipment;
        }

    }

    public function findByUser(int $id) {

        $this->id=$id;

        $sql="SELECT * FROM details
                WHERE user_id= :id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute([ 'id' => $this->id ]);
        $results = $stmt->fetchAll();
        
        $leasings = [];

        foreach( $results as $result){

            $leasing = new LeasingHouse( $result );
        
            $leasings[] = $leasing;
        }

        // var_dump($leasings);
        // die;
        return $leasings;

    }

    public function renameAndRedirectPhoto(array $img): string {

                        
        // On récupère l'extension pour vérification
        $ext = strtolower(substr($img['name'], -3));

        // On spécifie les extension autorisé
        $allow_ext = array('jpg','png','gif');

     
         // On vérifie le tous dans une condition
        if(in_array($ext,$allow_ext) ) {

            // On creer le chemin d'enregistrement et uniqid() se charge de transformer le nom en alphanumérique unique.                                
            $iname = uniqid() . '.' . $img['name'];
            // On envoie le tous
        
            move_uploaded_file($img['tmp_name'], "images/" . $iname);

            return $iname;

        }

        return '';

    }
   
}