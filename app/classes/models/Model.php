<?php

abstract class Model {

    protected $id;

    protected $bdd;

    private $photo;

    
    abstract protected function getTable(): string;
    abstract protected function toArray(): array;

    public function getPhoto() {
        return $this->photo;
    }

    public function setPhoto( $photo ) {        
        $this->photo = $photo;
        return $this;
    }
        
    //getter
    public function getId() {

        return $this->id;
    }

    //setter
    public function setId( $id ) {
        $this->id = $id;
        return $this->id;
        
    }

    public function __construct( array $datas = [] ) {

        $this->bdd = ( new Bdd );
        $this->hydrate( $datas );
    }

    protected function hydrate( array $datas ) {

        foreach( $datas as $key => $data ) {

            // On génère dynamiquement les méthodes setters
            $setter = 'set' . ucfirst($key);

            // On vérifie que la méthode généré existe
            if ( method_exists( $this, $setter) ) {
                 // On appelle la fonction sur $this a travers la variable $setter.
                 // $setter va être interprété puis la fonction executée.
                 $this->{$setter}( $data );
            }

        }
    }

    // Génére une chaine de type ':var1, :var2, :var3'
    private function getInsertSqlVars(): string {

        $str = '';
        foreach ($this->toArray() as $key => $value) {
            $str .= ':' . $key . ', ';
        }

        // supprime les caractères en fin de chaine
        $str = rtrim( $str, ', ' );

        return $str;

    }

    // fonction globale pour récupérer l'ensemble d'une table
    public static function findAll(): array {

        $classname = get_called_class();
        $ref = new $classname;

        $sql = "SELECT * FROM {$ref->getTable()}";
        $results = $ref->bdd
                        ->getPdo()
                        ->query( $sql )
                        ->fetchAll();

        $houses = [];

        foreach( $results as $result ) {
            $house = new $classname( $result );
            $houses [] = $house;
        }

        return $houses;

    }

    public function find( int $id ): bool {

        $this->id = $id;

        $sql = "SELECT * FROM {$this->getTable()} WHERE id=:id";
        $stmt = $this->bdd
                    ->getPdo()
                    ->prepare( $sql );
        $results = $stmt->execute(['id'=> $this->id ]);           
        $results = $stmt->fetch();
        
        if( !$results ) return false;
        
        $this->hydrate( $results );
        return true;

    }

    public function create(): bool {

        $vars = $this->getInsertSqlVars();
        $sql = "INSERT INTO {$this->getTable()} VALUES (0, {$vars})";
        $pdo = $this->bdd->getPdo();
        $stmt = $pdo->prepare( $sql );
        $stmt->execute( $this->toArray() );

        if( $stmt->rowCount() == 0 ) return false;

        $this->id = $pdo->lastInsertId();
        return true;

    }



    
}