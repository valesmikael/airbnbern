<?php

class Favories extends Model {

    private $detail_id;
    private $user_id;

    
    protected function getTable(): string {
        return 'favories';
    }
   
    protected function toArray(): array {
        return [            
            'detail_id' => $this->detail_id,
            'user_id' => $this->user_id                       
        ];
    }

       
    public function getDetail_id() {
        return $this->detail_id;
    }

    public function setDetail_id($detail_id) {
        $this->detail_id = $detail_id;

        return $this;
    }
 
    public function getUser_id() {
        return $this->user_id;
    }
    
    public function setUser_id($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    public function findByUser(int $id) {

        $this->id=$id;
        
        $sql="SELECT * FROM details 
                JOIN {$this->getTable()} as fav
                ON fav.detail_id = details.id
                WHERE fav.user_id =:id";
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute([ 'id' => $this->id ]);
        $results = $stmt->fetchAll();
        
        $favories = [];

        foreach( $results as $result){

            $favory = new Favories( $result );
        
            $favories[] = $favory;
        }

        return $favories;

    }
    
    
}