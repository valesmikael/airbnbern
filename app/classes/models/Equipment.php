<?php

class Equipment extends Model {

private $label;

protected function getTable(): string {
    return 'equipments';
}

protected function toArray(): array {
    return [ 'label' => $this->label ];
}

    
public function getLabel() {
    return $this->label;
}


public function setLabel($label) {
    $this->label = $label;

    return $this;
}


}