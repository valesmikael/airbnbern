<?php
class Auth {

    private $username;
    private $password;
    private $password_check;
    private $firstname;
    private $name;
    private $role_id;

    public function __construct( $username, $password, $password_check = '', $firstname = '', $name = '', $role_id = '' ) {
        $this->username = $username;
        $this->password = hash( 'sha256', $password );
        $this->password_check = hash( 'sha256', $password_check );
        $this->firstname = $firstname;
        $this->name = $name;
        $this->role_id = $role_id;
    }

    public function login(): ?User {

        $sql = 'SELECT * FROM users WHERE username=:username AND password=:password';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([
            'username' => $this->username,
            'password' => $this->password
            
        ]);

        $result = $stmt->fetch();

        if( !$result ) return null;

        // Créer un User -> rappel: le __construct fait appel à l'hydrate sur le tableau passé en paramètre
        $user = new User( $result );
        $_SESSION['user'] = $user;

        return $user;

    }

    public function checkPasswords(): bool {

        return $this->password == $this->password_check;

    }

    public function signin(): ?User {

        $user = new User;
        $user
            ->setUsername( $this->username )
            ->setPassword( $this->password )
            ->setFirstname( $this->firstname )
            ->setName( $this->name )
            ->setRoleId( $this->role_id );

        if( $user->create() ) {
            $_SESSION['user'] = $user;
            return $user;
        }
        else return null;

    }
 
    public function userExists():bool {

        $sql = 'SELECT id FROM users WHERE username=:username';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute(['username'=> $this->username]);

        return $stmt->rowCount() > 0;

    }

    public static function isLogged(): bool {
        return isset( $_SESSION['user'] );
    }

    public static function user(): ?User {
        return self::islogged() ? $_SESSION['user'] :null;
    }
}