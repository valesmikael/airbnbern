<?php

// inclure le fichier du router
require_once 'app/flight/Flight.php';

// inclure l'autoload
require_once 'autoload.php';

// inclure le fichier config pour lier les constantes
require_once 'CONFIG.php';

// démarrage de la session
session_start();

$authController = new AuthController;
Flight::route('/authentication', [ $authController, 'form' ]);
Flight::route('POST /login', [ $authController, 'login' ]);
Flight::route('POST /signin', [ $authController, 'signin' ]);
Flight::route('/unlogin', [ $authController, 'unlogin' ]);


$appController = new AppController;
Flight::route('/', [$appController, 'index']);

$detailController = new DetailController;
Flight::route('/leasing/detail/@id:[0-9]*', [$detailController, 'show']);

$newHouseController = new NewHouseController;
Flight::route('/leasing/create', [$newHouseController, 'create']);
Flight::route('POST /leasing/store', [$newHouseController, 'store']);

$dashboardAdvertiserController = new DashboardAdvertiserController;
Flight::route('/dashboard/advertiser',[$dashboardAdvertiserController, 'index']);

$dashboardStandardController = new DashboardStandardController;
Flight::route('/dashboard/standard',[$dashboardStandardController, 'index']);

$reservedController = new ReservedController;
Flight::route('/leasing/reserved/@id:[0-9]*',[$reservedController, 'store']);

$favoriesController = new FavoriesController;
Flight::route('/leasing/favories/@id:[0-9]*',[$favoriesController, 'store']);



// lancement du router
Flight::start();